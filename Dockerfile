FROM golang:latest

ADD . /home
WORKDIR /home

RUN go build golang.go

CMD = ["/home/golang"]
